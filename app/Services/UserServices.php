<?php

namespace App\Services;


use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class UserServices
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return Builder
     */
    public function getUser() : Builder
    {
        return $this->userRepository->getAllByDesk();
    }

    /**
     * @param string $name
     * @return Builder[]|Collection
     */
    public function searchByName(string $name)
    {
        return $this->userRepository->getAllByName($name);
    }
}
