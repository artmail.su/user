<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Search\UserSearchRequest;
use App\Http\Resources\UserListResource;
use App\Services\UserServices;

class UserController extends Controller
{
    private $service;

    public function __construct(UserServices $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $usersQuery = $this->service->getUser();
        $users = $usersQuery->paginate(10);

        return UserListResource::collection($users);
    }

    public function search(UserSearchRequest $request)
    {
        $users = $this->service->searchByName($request['name']);

        return UserListResource::collection($users);
    }
}
