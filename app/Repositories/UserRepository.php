<?php

namespace App\Repositories;


use App\Entity\User\User;

class UserRepository
{
    public function getAllByName(string $name)
    {
        return User::query()->where('name', 'LIKE', "%{$name}%")->get();
    }

    public function getAllByDesk()
    {
        return User::query()->orderByDesc('id');
    }
}
